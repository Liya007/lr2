class corpus ():
    '''  класс, обеспечивающий обработку набора документов (корпуса документов) '''

    def __init__(self):
        '''иниицализируем пустой список документов'''
        self.documents = []

    def add_document(self, document):
        ''' добавление документа в корпус'''
        self.documents.append(document)

    def build_vocabulary(self):
        ''' получаем словарь терминов на основе всего корпуса '''
        discrete_set = set()
        for document in self.documents:
            for word in document.trems_doc:
                discrete_set.add(word)
        self.vocabulary = list(discrete_set)


