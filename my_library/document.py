import re
from nltk.stem import SnowballStemmer
import nltk
from nltk.util import ngrams
from collections import Counter

class Document(object):
    '''
    Класс, содрежащий словарь терминов по каждому документу
    '''
    stop_words = nltk.corpus.stopwords.words('russian')
    stemmer = SnowballStemmer('russian')

    def __init__(self, filepath):
        '''сразу получаем сырой текст из файла'''
        self.trems_doc = []

        handle = open(filepath, "r", encoding="utf-8")
        self.text = handle.read()
        print('got raw data : ' + self.text)
        handle.close()

    def remove_unnecessary_characters(self):
        '''убираем ненужные символы'''
        self.text = re.sub(r'[\W]+', ' ', str(self.text).lower())
        self.text = re.sub(r'[\d]+', ' ', self.text)
        return self.text

    def stemming(self):
        ''' СЕЕММИНГ || не очень хорошо унифицирует слова (лучше заменить на лемматизацию)'''
        stem = [Document.stemmer.stem(w) for w in self.text.split()]
        stem = ' '.join(stem)
        self.text = stem
        return self.text

    def deleting_stop_words(self):
        ''' удвление стоп слов '''
        text_splited = self.text.split()
        result = ''
        for item in text_splited:
            if item not in Document.stop_words:
                result = result + ' ' + item
        self.text = result
        return self.text

    def get_ngram_list(self):
        ''' получение списка терминов (грам, биграм и триграм) '''

        # удаляем ненужные символы
        self.remove_unnecessary_characters()

        # удаляем стоп-слова
        self.deleting_stop_words()

        # осуществляем стемминг
        self.stemming()

        token = nltk.word_tokenize(self.text)
        bigrams = ngrams(token, 2)
        trigrams = ngrams(token, 3)

        self.trems_doc = self.text.split()

        for k1, k2 in Counter(bigrams):
            self.trems_doc.append(k1 + "_" + k2)

        for k1, k2, k3 in Counter(trigrams):
            self.trems_doc.append(k1 + "_" + k2 + "_" + k3)

        return self.trems_doc
